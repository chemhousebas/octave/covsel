function [sep,pred,model] = covsel_mlr(calx,caly,testx,testy,nf,centx,centy,fonc)
%   function [sep,pred,model] = covsel_mlr(calx,caly,testx,testy,nv,centx,centy,func)
%
%   apply on testx and testy a CovSel-R model calibrated on calx and caly
%   calx : calibration X
%   caly : calibration Y
%   testx: test X
%   testy: test Y
%   centx : optional parameter (numeric or alpha) : 
%       if 2, X is autoscaled
%       if 1, X is centered (1 by default)
%       if 0, X is not centered 
%       if alphabetic, specify the centering function. This function will
%           receive the x and y and should return the x-centre and scale
%           e.g: [xm,sm]= center_medx(x,y)
%                   xm = median(x);
%                   sm = x(1,:).*0+1;
%                  
%   centy : optional parameter (numeric or alpha) : 
%       if 2, Y is autoscaled
%       if 1, Y is centered (1 by default)
%       if 0, Y is not centered (model without intercept)
%       if alphabetic, specify the centering function. This function will
%           receive the x and y and should return the y-centre and scale
%           e.g: [ym,ys]= centre_medy(x,y)
%                   ym = median(y);
%                   ys = y(:,1).*0+1;
%
%   func : preprocessing [xc,yc]=fonc(calx,caly,testx,testy);
%
%   sep : RMSE for 1.. nv variables
%   pred : matrix of predictions
%   model : structure containing informations about the model
%
%   This function can be used with xxcvn
%
%  JM Roger Irstea ITAP, France

if size(calx,1) ~= size(caly,1) | size(testx,1) ~= size(testy,1)
    error('Number os samples do not match');
end;
if size(calx,2) ~= size(testx,2) | size(caly,2) ~= size(testy,2)
    error('Number os variables do not match');
end;


[n,p] = size(calx);
[m,p] = size(testx);
[n,q] = size(caly);

sep = zeros(q, nf);
pred = zeros(m, q, nf);

if nargout>2
    model.bs = sep;
    model.reg = zeros(p, q, nf);
end;

switch nargin
    case 5
  centx=1;
  centy=1;
  fonc = '';
    case 6
  centy=1;
  fonc = '';
    case 7
  fonc = '';
end;

% pretreatment
if ~isempty(fonc)
    eval(sprintf('[calx,testx]=%s(calx,caly,testx,testy);',fonc));
end;

% scaling 
xm = calx(1,:).*0;
ym = caly(1,:).*0;
xs = calx(1,:).*0+1;
ys = caly(1,:).*0+1;

if isnumeric(centx)
    if centx>=1
      xm=mean(calx);
    end
    if centx==2
      xs=std(calx);
    end;
else
    eval(sprintf('[xm,xs]=%s(calx,caly);',centx));
end;

if isnumeric(centy)
    if centy>=1
      ym=mean(caly);
    end
    if centy==2
      ys=std(caly);
    end;
else
    eval(sprintf('[ym,ys]=%s(calx,caly);',centy));
end;

calx = (calx-repmat(xm,size(calx,1),1))./repmat(xs,size(calx,1),1);
testx = (testx-repmat(xm,size(testx,1),1))./repmat(xs,size(testx,1),1);
caly = caly-repmat(ym,size(caly,1),1);

% model calculation
  % selection
num = covsel(calx,caly,nf,0,0);
  % regression
b = zeros(p,q,nf);
for i=1:nf
  bmlr = calx(:,num(1:i)) \ caly;
  b(num(1:i),:,i) = bmlr;
end

% prediction
for j = 1:nf
    pr = testx*b(:,:,j) + repmat(ym,size(testy,1),1);
    pred(:,:,j) = pr;
    press(:,j) = sum((pr-testy).^2);
end

sep = sqrt(press ./ size(testy,1));


if nargout > 2
    % bias / sep
    model.bs = squeeze(mean( pred - repmat(testy,[1,1,nf])));
    model.sep = squeeze(sep);
    
    % predictions
    model.pred = squeeze(pred);
    
    % reg coeff
    model.reg = b;
    
    % selection
    model.selection = num;
end;
 
pred = squeeze(pred);
sep = squeeze(sep);


