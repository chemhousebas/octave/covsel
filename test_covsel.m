
function test_covsel
% test on cepage data

input('Press enter key to start demo on cepage data');

clear all
switch computer
    case 'PC'
        load('data\data_cepages_cal_test.mat');
    otherwise
        load('data/data_cepages_cal_test.mat');
end;

% run covsel between spectra and 3 classes
[num,crbvar,xdef,ydef,crbcov] = covsel(x,y,6);

% visualize the selection
figure;
xm =inv(y'*y)*y'*x;
mx = max(xm(:));
mn = min(xm(:));
plot(lambda,xm); % plot the mean spectra per class
hold on;
for i=1:length(num)
    plot([lambda(num(i)),lambda(num(i))],[mn mx],'k');
    set(text(lambda(num(i)), mx, [' ' num2str(i)]),'fontsize',12);
end
hold off

% visualize the evolution of covariance
figure;
for i=1:6
    subplot(3,2,i);
    plot(lambda,crbcov(i,:));
    axis tight
    hold on;
    for j=1:i
        plot([lambda(num(j)),lambda(num(j))],get(get(gcf,'currentax'),'ylim'),'k');
    end
    hold off
    title(['step ' num2str(i)])
    if mod(i,2)==1
        ylabel('cov(x_i,y)^2')
    end
end;
   

% test on corn data
input('Press enter key to start demo on corn data');
clear all
switch computer
    case 'PC'
        load('data\data_corn_cal_test.mat');
    otherwise
        load('data/data_corn_cal_test.mat');
end;

% moisture (first column of y)
% calibration on xc, test on xv, for 1:15 variables
[sep,pred,model] = covsel_mlr(xc,yc(:,1),xv,yv(:,1),15);

figure
plot(sep); legend('SEP')

figure
plot(yv(:,1),pred(:,8),'*');
corr(yv(:,1),pred(:,8))

disp(sprintf(' SEP=%.3f Bias=%.3f R^2=%.3f',sep(8),model.bs(8),corr(yv(:,1),pred(:,8))^2));

disp(['optimal selection : ' sprintf('%.0f nm, ',lbd(model.selection(1:8)))])

% all columns
% calibration on xc, test on xv, for 1:10 variables
[sep,pred,model] = covsel_mlr(xc,yc,xv,yv,10);

figure
% RPD calculation
rpd = repmat(std(yc)',1,10) ./ sep;
plot(rpd'); legend('RPD moisture','RPD fat','RPD protein','RPD starch')

disp(['optimal selection : ' sprintf('%.0f nm, ',lbd(model.selection(1:8)))])

model

